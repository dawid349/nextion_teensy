/*
 * nextion.c
 *
 *  Created on: Apr 11, 2020
 *      Author: Dawid
 */

#include "nextion.h"
#include <math.h>
#include <stdlib.h>

#define BUF_LENGTH 400



uint8_t nextion_buf[BUF_LENGTH]="";		// the main tx_buffer
uint16_t size2=0;		// index and final length of whole data frame - iportant variable!!!

volatile uint8_t left_ind_count=0;	// variable for necessary correct order of animation of direction lights
volatile uint8_t right_ind_count=0;
volatile bool emerg_light_event_detect[2]={0,0}; //buffer use for detecting hazard light event (power off or power on)
volatile uint8_t emerg_index=0; //index of buffer of detect event
//uint8_t vel_index=0;	// variable necessary for correct animation of velocity
uint8_t power_current_pic=0; // current picture of POWER PROGRESS BAR
uint8_t power_first_pic=0;	 // pictures set default for NORMAL MODE
uint8_t power_last_pic=40;
uint8_t left_ind_first_pic=47;   // necessary for indicators animation, set default for NORMAL MODE
uint8_t right_ind_first_pic=51;
uint8_t ind_left_off=46;
uint8_t ind_right_off=50;
uint8_t table_anim_angle[17]={85,82,80,78,76,74,72,70,69,71,73,75,77,79,81,83,84}; //look up table for angle animation
uint8_t indicators_delay=0;		// for delay refreshing indicators and hazard lights data


/*deal with page changing*/
uint8_t current_page_detect[2]={0,0}; //table to remember last two states of page button
uint8_t page_index=0;                 // index of table above

/*animation of battery level at battery charging page*/
uint8_t first_battery_pic=236;
uint8_t last_battery_pic=246;
uint8_t current_battery_pic=0;


NextionData message={0};


void static reset_buf(uint8_t* buf)
{
	for(uint8_t i=0;i<sizeof(buf);i++)
	{
		buf[i]=0;

	}

}

static void Ind_Left(bool state,bool emerg_light_state)			// function is changing proper pictures in proper direction
{
	if(emerg_light_state==false)
	{
	if(state==true)
	{

		if(left_ind_count>2)
		{
			left_ind_count=0;
		}
		uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",IND_L_ON,left_ind_first_pic+left_ind_count);
		END_SEPARATOR
		left_ind_count++;
	}
	else
	{
		uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",IND_L_ON,ind_left_off);
		END_SEPARATOR
		left_ind_count=0;
	}
	}

}


static void Ind_Right(bool state,bool emerg_light_state)						// function is changing proper pictures in proper direction
{

	if(emerg_light_state==false)
	{


	if(state==true)
	{

		if(right_ind_count>2)
		{
			right_ind_count=0;
		}
		uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",IND_R_ON,right_ind_first_pic+right_ind_count);
		END_SEPARATOR
		right_ind_count++;
	} //end of if
	else
	{
		uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",IND_R_ON,ind_right_off);
		END_SEPARATOR
		right_ind_count=0;

	}

	}
}

static void Prk_Lights(bool state)
{
	uint8_t size=0;

	if(state==true)
	{
		size=sprintf(((char*)nextion_buf+size2),"%s",PARK_LIGHTS_ON);
	}
	else
	{
		size=sprintf(((char*)nextion_buf+size2),"%s",PARK_LIGHTS_OFF);
	}

	END_SEPARATOR


}


static void Drv_Lights(bool state)
{
	uint8_t size=0;

	if(state==true)
	{
		size=sprintf(((char*)nextion_buf+size2),"%s",DRV_LIGHT_ON);
	}
	else
	{
		size=sprintf(((char*)nextion_buf+size2),"%s",DRV_LIGHT_OFF);
	}

		//HAL_UART_Transmit_IT(&huart2,nextion_buf, size+3);
	END_SEPARATOR

}

static void Beam_Lights(bool state)
{
	uint8_t size=0;

	if(state==true)
	{
		size=sprintf(((char*)nextion_buf+size2),"%s",BEAM_LIGHT_ON);
	}
	else
	{
		size=sprintf(((char*)nextion_buf+size2),"%s",BEAM_LIGHT_OFF);
	}

	END_SEPARATOR


}

static void Hazard_Lights(bool state)							// function linked with indicators
{

		emerg_light_event_detect[emerg_index]=state;		//remember two last states of hazard lights
		emerg_index++;
		if(emerg_light_event_detect[0]!=emerg_light_event_detect[1]) //if event occured---if state was changed
		{
			left_ind_count=0;	// variable for necessary correct order of animation of direction lights
			right_ind_count=0;  // reset positions of direction lights to start animation from middle again
		}

		if(emerg_index>1)
		{
			emerg_index=0;
		}

		uint8_t size=0;

		if(state==true)			// if true take control on direction lights
		{
			size=sprintf(((char*)nextion_buf+size2),"%s",HAZARD_ON);
			END_SEPARATOR
			Ind_Right(1,0);
			Ind_Left(1,0);
		}
		else
		{
			size=sprintf(((char*)nextion_buf+size2),"%s",HAZARD_OFF);
			END_SEPARATOR
		}


}


static void BT_state(bool state)
{
	uint8_t size=0;

		if(state==true)
		{
			size=sprintf(((char*)nextion_buf+size2),"%s",BT_ON);
		}
		else
		{
			size=sprintf(((char*)nextion_buf+size2),"%s",BT_OFF);
		}

		END_SEPARATOR
}


static void Current_Page(uint8_t page)
{

	current_page_detect[page_index]=page;		//remember two last numbers of page
	page_index++;
	if(current_page_detect[0]!=current_page_detect[1]) //if event occured---if page was changed
	{
		uint8_t size=0;

		//reset_buf(nextion_buf);

		switch(page)
		{
			case 0:			// MAIN PAGE
			{
				reset_buf(nextion_buf);     // reset tx buffer (only message of page changing is sent)
				size2=0;					// reset index of buffer (important)+
				size=sprintf(((char*)nextion_buf+size2),"%s%d",PAGE,page);
				END_SEPARATOR
			}
			break;
			case 3:			// PAGE OF BATTERY CHARGING AND OTHER PARAMETERS
			{
				reset_buf(nextion_buf);
				size2=0;
				size=sprintf(((char*)nextion_buf+size2),"%s%d",PAGE,page);
				END_SEPARATOR

			}
			break;
			case 1:			// PAGE WITH MORE PARAMETERS
			{
				reset_buf(nextion_buf);
				size2=0;
				size=sprintf(((char*)nextion_buf+size2),"%s%d",PAGE,page);
				END_SEPARATOR
			}
			default:		// default do nothing
			{

			}
			break;
		}
		//Send_all_data();

	}

	if(page_index>1)
	{
		page_index=0;
	}

}


static void Drive_mode(uint8_t mode)
{
	uint8_t size=0;
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",DRIVE_MODE_CHANGE,mode);
	END_SEPARATOR

	switch(mode)
	{
		case 1: // NORMAL_MODE
		{
			power_first_pic=0;	 // pictures set default for NORMAL MODE
		    power_last_pic=40;
			left_ind_first_pic=47;   // necessary for indicators animation, set default for NORMAL MODE
			right_ind_first_pic=51;
			ind_left_off=46;
			ind_right_off=50;
		}
		break;

		case 0:	//ECO MODE
		{
			power_first_pic=101;	 // pictures set default for NORMAL MODE
			power_last_pic=141;
			left_ind_first_pic=94;   // necessary for indicators animation, set default for NORMAL MODE
			right_ind_first_pic=90;
			ind_left_off=93;
			ind_right_off=89;
		}
		break;

		case 2:	// DYNAMIC_MODE
		{
			power_first_pic=158;	 // pictures set default for NORMAL MODE
			power_last_pic=194;
			left_ind_first_pic=150;   // necessary for indicators animation, set default for NORMAL MODE
			right_ind_first_pic=154;
			ind_left_off=149;
			ind_right_off=153;
		}
		break;
	}
}


static void Battery_level(float level)
{
	/*numeral value in percents*/
	int int_level=level;
	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",BATTERY_LEVEL,int_level);
	END_SEPARATOR
	/*animation of battery level*/
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",BATTEY_LVL_ANIM,int_level);
	END_SEPARATOR

}

static void Battery_temp(float temp)
{
	int int_temp=temp;
	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",BATTERY_TEMP,int_temp); // add value of battery_temp to buffer
	END_SEPARATOR
}

static void Engine_temp(float temp)
{
	int int_temp=temp;
	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",ENGINE_TEMP,int_temp); // add value of battery_temp to buffer
	END_SEPARATOR
}

static void Velocity(float velocity)
{

	uint8_t vel_int=velocity;				//velocity in kilometers per hour
	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",VELOCITY,(int)vel_int); // add value to buffer

	END_SEPARATOR

}

static void Angle(float angle)
{
	int8_t int_angle=angle;

	if((abs(int_angle)>80) && (angle>0))		// to avoid break limit of pictures number !!!
	{
		int_angle=80;
	}

	if((abs(int_angle)>80)&&(angle<0))		// to avoid break limit of pictures number !!!
	{
		int_angle=-80;
	}

	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",ANGLE,(int)(abs((floor(int_angle/10)*10))));

	END_SEPARATOR

	uint8_t angle_current_pic=table_anim_angle[(int)(floor(int_angle/10)+8)];		//look up table for animation


	size=sprintf(((char*)nextion_buf+size2),"%s=%d",ANGLE_ANIM,angle_current_pic);

	END_SEPARATOR


}

static void Power(float torque, float velocity)
{

	int EnginePower=(int)(torque*velocity); // velocity in [rad/s], torque in [N*m]

	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",POWER,EnginePower);

	END_SEPARATOR

	/*PROGRESS BAR OF POWER*/

	power_current_pic=power_first_pic+(int)(EnginePower/1.25); //assuming MAX POWER of engine as 50 [KW]

	if(power_current_pic<power_first_pic)		//check number of picture to avoid breaking the limits
	{
		power_current_pic=power_first_pic;
	}

	else if(power_current_pic>power_last_pic)
	{
		power_current_pic=power_last_pic;
	}

	size=sprintf(((char*)nextion_buf+size2),"%s=%d",POWER_ANIM,power_current_pic);

	END_SEPARATOR

}

static void Error(bool state)
{
	uint8_t size=0;

		if(state==true)
		{
			size=sprintf(((char*)nextion_buf+size2),"%s",ERROR_ON); // ERROR marker on
		}
		else
		{
			size=sprintf(((char*)nextion_buf+size2),"%s",ERROR_OFF); // ERROR marker off
		}

		END_SEPARATOR

}

static void Send_all_data()
{
	//Serial1.begin(9600);
	Serial1.write(nextion_buf, size2);
	size2=0;
}

/*FUNCTIONS FOR PAGE WITH ADDITIONAL PARAMETERS*/
static void Send_Additional_Parameters(NextionData* data)
{

	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",MAX_BAT_VOLTAGE,data->Battery_Max_Voltage);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",MEAN_BAT_VOLTAGE,data->Battery_Mean_Voltage);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",MIN_BAT_VOLTAGE,data->Battery_Min_Voltage);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",BEST_ACCEL,data->Best_Acceleration);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",LAST_ACCEL,data->Last_Acceleration);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",MAX_POWER,data->Max_Power);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",MAX_VELOCITY,data->Max_Velocity);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",MILEAGE,data->Mileage);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",BEST_RECHARGE,data->Best_Recharge);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",MAX_RUNTIME,data->Max_Runtime);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",TRAVEL_TIME,data->Travel_Time);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",DISTANCE,data->Distance);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",EST_DIST_LEFT,data->Est_dist_left);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",BAT_PERCENT_LVL,(int)(data->Battery_Level));
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=%d",BRIGHTNESS_LVL,data->Brightness);
	END_SEPARATOR

}

static void Set_Time_And_Data(NextionData* data)
{
	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=\"%02d.%02d.%d\"",LAST_RECHARGE,data->Last_Recharge_Day,data->Last_Recharge_Month,data->Last_Recharge_Year);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=\"%02d.%02d.%d\"",CURRENT_DATE,data->Current_Day,data->Current_Month,data->Current_Year);
	END_SEPARATOR
	size=sprintf(((char*)nextion_buf+size2),"%s=\"%02d:%02d\"",CURRENT_TIME,data->Current_Hour,data->Current_Minute);
	END_SEPARATOR
	//"t0.txt=\"NORMAL\""

}

/*FUNCTIONS FOR BATTERY CHARGING PAGE*/
static void Set_Battery_Charge_Parameters(NextionData* data)
{
	uint8_t size=sprintf(((char*)nextion_buf+size2),"%s=%d",BAT_PERCENT_LVL_2,(int)(data->Battery_Level));
	END_SEPARATOR

	size=sprintf(((char*)nextion_buf+size2),"%s=\"%02d:%02d\"",CURRENT_TIME_2,data->Current_Hour,data->Current_Minute);
	END_SEPARATOR

	size=sprintf(((char*)nextion_buf+size2),"%s=\"%02d:%02d\"",TIME_OF_FULL_CHARGE,data->Hour_of_full_charge,data->Minute_of_full_charge);
	END_SEPARATOR

	current_battery_pic=first_battery_pic+(int)((data->Battery_Level)/10.0);

	if(current_battery_pic<first_battery_pic)		//check number of picture to avoid breaking the limits
	{
		current_battery_pic=first_battery_pic;
	}

	else if(current_battery_pic>last_battery_pic)
	{
		current_battery_pic=last_battery_pic;
	}

	size=sprintf(((char*)nextion_buf+size2),"%s=%d",BAT_PERCENT_LVL_2_ANIM,current_battery_pic);

	END_SEPARATOR

}

static void Send_Nextion_Data(NextionData* data)
{

	  switch(data->Current_Page)
	  {
	  	  case 0:		// MAIN PAGE
	  	  {

		  	Drive_mode(data->Drive_Mode);

		  	indicators_delay++;
		  	if(indicators_delay>2)
		  	{
		  		Hazard_Lights(data->Hazard_Light);
	  			Ind_Left(data->Ind_Left,data->Hazard_Light);
	  	    	Ind_Right(data->Ind_Right,data->Hazard_Light);
	  	    	indicators_delay=0;
		  	}

	  	    /* for the rest of ligts*/
	  	    Drv_Lights(data->Drv_lights);
	  	    Beam_Lights(data->Beam_Lights);
	  	    Prk_Lights(data->Prk_Lights);

	  		Error(data->ERROR);
	  		/*bluetooth*/
	  		BT_state(data->Bt_State);
	  		/*parameters*/
	      	Battery_level(data->Battery_Level);
	  		Battery_temp(data->Battery_Temp);
	  		Engine_temp(data->Engine_Temp);
	  		Velocity(data->Velocity);
	  		Angle(data->Angle);
	  		Power(1,data->Rotation_Velocity);

	  	  }
	  break;
	  	  case 1:  // PAGE WITH MORE PARAMETERS
	  	  {
	  		Drive_mode(data->Drive_Mode);
	  		Send_Additional_Parameters(data);
	  		Set_Time_And_Data(data);

	  	  }
	  break;

	  	  case 3:	// PAGE OF BATTERY CHARGING
	  	  {
	  		Drive_mode(data->Drive_Mode);
	  	    Set_Battery_Charge_Parameters(data);
	  	  }
	  break;


	  }
	  Current_Page(data->Current_Page); //should be called at THE END -- IMPORTANT!!!
	  Send_all_data();


}

void Nextion_Loop()
{

	message.Current_Page=0;				// current page on screen
	message.Drive_Mode=1;  				// current drive mode

	/*LIGHTS*/
	message.Hazard_Light=true;			// current hazard light state
	message.Ind_Left=true;				// current state of left indicator
	message.Ind_Right=false;			// current state of right indicator
	message.Drv_lights=false;			//current state of driving lights
	message.Beam_Lights=false;			// current state of beam lights
	message.Prk_Lights=true;			// current state os=f park lights

	/*DIAGNOSTICS AND BLUETOOTH STATE*/
	message.ERROR=true;					// state of error marker
	message.Bt_State=true;				// state of bluetooth connection (connected or not connected)

	/*PARAMETERS*/
	message.Battery_Level=1.432;		// battery level in [%]
	message.Battery_Temp=2.231;		    // battery temperature in [Celsius Grad]
	message.Engine_Temp=1.324;			// engine temperature in [Celsius Grad]
	message.Velocity=0.423;			    // velocity in [km/h]
	message.Angle=-90.231;				// roll angle in degrees
	message.Rotation_Velocity=50;	    // signal from throttle in range 0 to 50, it is used for power progress bar

	/*PARAMETERS FOR ADDITIONAL PAGE*/
	message.Battery_Max_Voltage=99;       // [V]
    message.Battery_Mean_Voltage=85.321;  // [V]
	message.Battery_Min_Voltage=67.21;    // [V]
	message.Best_Acceleration=1;          // [km/h] ???
	message.Last_Acceleration=2;	      // [km/h] ???
	message.Max_Power=40.321;			  // [kW]
	message.Max_Velocity=89.312;		  // [km/h]
	message.Mileage=100.32;				  // [km/h]
	message.Best_Recharge=21.32;		  // [km/h]
	message.Max_Runtime=2.3;			  // [h]
	message.Travel_Time=15;               // [min]
	message.Brightness=33;                // brightness is scale from 0 to 100
    message.Distance=100; 				      // [km]
    message.Est_dist_left=99;                // [km]

	/*CURRENT AND LAST RECHARGE DATE PARAMETERS*/
	message.Current_Day=2;
	message.Current_Month=12;
	message.Current_Year=2020;
	message.Last_Recharge_Day=3;
	message.Last_Recharge_Month=3;
	message.Last_Recharge_Year=2012;

	/*BATTERY CHARGING PAGE*/
	message.Hour_of_full_charge=3;
	message.Minute_of_full_charge=12;
	message.Current_Hour=12;
	message.Current_Minute=13;


	Send_Nextion_Data(&message);


}
