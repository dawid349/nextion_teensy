/*
 * nextion.h
 *
 *  Created on: Apr 11, 2020
 *      Author: Dawid
 */

#ifndef INC_NEXTION_H_
#define INC_NEXTION_H_
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#define END_SEPARATOR nextion_buf[size2+size]=255;nextion_buf[size2+size+1]=255;nextion_buf[size2+size+2]=255;size2=size2+size+3;
/*MAIN PAGE*/

/* values of parameters*/
#define		POWER				"n1.val"			//display value of engine power
#define		POWER_ANIM			"p8.pic"			//display current picture of power progress bar
#define		VELOCITY			"n2.val"			//display current value of velocity
#define 	ANGLE				"n0.val"			//display current angle of our motorcycle
#define 	ANGLE_ANIM			"p9.pic"			//display picture of angle animation depending on current angle value
#define		BATTERY_LEVEL  	    "n5.val" 			//display value of battery level in percents
#define		BATTEY_LVL_ANIM 	"j0.val" 			//animation of battery level
#define		BATTERY_TEMP		"n4.val"			//display value of battery_temp
#define		ENGINE_TEMP			"n3.val"			//display value of engine_temp
/* CHANGING DRIVE MODE*/


/* picture objects*/

/*LIGHTS WIHTHOUT DIRECTION LIGHTS*/
#define     HAZARD_ON			"p0.pic=61"
#define		HAZARD_OFF			"p0.pic=60"
#define		DRV_LIGHT_ON		"p1.pic=63"
#define		DRV_LIGHT_OFF		"p1.pic=62"
#define		BEAM_LIGHT_ON		"p2.pic=65"
#define		BEAM_LIGHT_OFF		"p2.pic=64"
#define		PARK_LIGHTS_ON		"p3.pic=59"
#define		PARK_LIGHTS_OFF		"p3.pic=58"
#define		ERROR_ON			"p5.pic=68"
#define		ERROR_OFF			"p5.pic=248"
/*BLUETOOTH*/
#define		BT_ON				"p7.pic=67"
#define		BT_OFF				"p7.pic=66"

/* DIRECTION LIGHTS*/
#define		IND_L_ON			"p4.pic"  // reference to indicators object
#define		IND_R_ON			"p6.pic"

/*CURRENT PAGE AND DRIVE MODE*/
#define		PAGE				"page "		// current page on display
#define     DRIVE_MODE_CHANGE	"va2.val"	// change pictures appropriate to current drive mode


/*PAGE WITH MORE OVERALL PARAMETERS*/

/*BAT VOLTAGE AND ACCELERATION*/

#define		MAX_BAT_VOLTAGE     "n6.val"
#define		MEAN_BAT_VOLTAGE    "n7.val"
#define		MIN_BAT_VOLTAGE		"n8.val"
#define		BEST_ACCEL			"n9.val"
#define		LAST_ACCEL			"n10.val"

/* ALL-TIME */

#define		MAX_POWER			"n11.val"
#define		MAX_VELOCITY		"n12.val"
#define		MILEAGE				"n13.val"
#define		BEST_RECHARGE		"n14.val"
#define		MAX_RUNTIME			"n15.val"

/* CURRENT */

#define		LAST_RECHARGE		"t10.txt"
#define		TRAVEL_TIME			"n17.val"
#define		DISTANCE			"n18.val"
#define		EST_DIST_LEFT		"n19.val"
#define		BRIGHTNESS_LVL		"h0.val"
#define		BAT_PERCENT_LVL		"nl.val"
#define		CURRENT_DATE		"t7.txt"
#define		CURRENT_TIME		"t8.txt"

/*BATTERY CHARGING PAGE*/
#define		BAT_PERCENT_LVL_2	   "n27.val"
#define		BAT_PERCENT_LVL_2_ANIM "p74.pic"
#define		CURRENT_TIME_2		   "t21.txt"
#define     TIME_OF_FULL_CHARGE	   "t20.txt"

/*DATA STRUCTURE*/

typedef struct{
	uint8_t Current_Page;		// current page on screen
	uint8_t Drive_Mode;  		// current drive mode

	/*LIGHTS*/
	bool Hazard_Light;		// current hazard light state
	bool Ind_Left;			// current state of left indicator
	bool Ind_Right;			// current state of right indicator
	bool Drv_lights;		//current state of driving lights
	bool Beam_Lights;		// current state of beam lights
	bool Prk_Lights;		// current state os=f park lights

	/*DIAGNOSTICS AND BLUETOOTH STATE*/
	bool ERROR;				// state of error marker
	bool Bt_State;			// state of bluetooth connection (connected or not connected)

	/*PARAMETERS*/
	float Battery_Level;	// battery level in [%]
	float Battery_Temp;		// battery temperature in [Celsius Grad]
	float Engine_Temp;		// engine temperature in [Celsius Grad]
	float Velocity;			// velocity in [km/h]
	float Angle;			// roll angle in degrees
	float Torque;			// current torque in [Nm]
	float Rotation_Velocity; // current engine rotation velocity in [rad/s]

	/*PARAMETERS FOR ADDITIONAL PAGE*/
	uint8_t  Battery_Max_Voltage;    // [V]
	uint8_t  Battery_Mean_Voltage;   // [V]
	uint8_t  Battery_Min_Voltage;    // [V]
	int8_t   Best_Acceleration;      // [km/h^2] ???
	int8_t   Last_Acceleration;	     // [km/h^2] ???
	uint8_t  Max_Power;			     // [kW]
	uint8_t  Max_Velocity;			 // [km/h]
	uint16_t Mileage;				 // [km/h]
	uint16_t Best_Recharge;		     // [km/h]
	uint8_t  Max_Runtime;			 // [h]
	uint16_t Travel_Time;            // [min]
	uint8_t  Brightness;             // brightness is scale from 0 to 100
    uint16_t Distance; 				 // [km]
    uint16_t Est_dist_left;          // [km]

	/*CURRENT AND LAST RECHARGE DATE PARAMETERS*/
	uint8_t	Current_Day;
	uint8_t Current_Month;
	uint16_t Current_Year;
	uint8_t Last_Recharge_Day;
	uint8_t Last_Recharge_Month;
	uint16_t Last_Recharge_Year;
	uint8_t Current_Hour;
	uint8_t Current_Minute;

	/*BATTERY CHARGING PAGE*/
	uint8_t Hour_of_full_charge;
	uint8_t Minute_of_full_charge;


}NextionData;





/*fuctions*/

void Nextion_Loop();

#endif /* INC_NEXTION_H_ */
